### Ruby on Rails template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/rails).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.

Por convención Rails va a buscar un archivo que se llame igual al método (ignorando la extensión) y que se encuentre dentro de una carpeta que se llame igual al controlador. Es por eso que creamos la carpeta pages y llamamos a la vista home.html.erb.
